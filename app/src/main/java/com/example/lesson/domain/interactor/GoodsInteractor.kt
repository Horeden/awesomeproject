package com.example.lesson.domain.interactor

import com.example.lesson.domain.model.goods.Goods
import com.example.lesson.domain.repository.GoodsRepository

class GoodsInteractor(private val goodsRepository: GoodsRepository) {

    suspend fun getGoods(): List<Goods> {
        return goodsRepository.getGoods()
    }
}