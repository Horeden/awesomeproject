package com.example.lesson.domain.model.goods

data class Goods(val plu: String,
            val price: Double,
            val name: String) {

}