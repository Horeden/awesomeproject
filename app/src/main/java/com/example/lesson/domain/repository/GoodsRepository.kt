package com.example.lesson.domain.repository

import com.example.lesson.domain.model.goods.Goods

interface GoodsRepository {
    suspend fun getGoods(): List<Goods>
}