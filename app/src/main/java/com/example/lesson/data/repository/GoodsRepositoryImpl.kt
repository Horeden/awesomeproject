package com.example.lesson.data.repository

import com.example.lesson.domain.model.goods.Goods
import com.example.lesson.domain.repository.GoodsRepository

class GoodsRepositoryImpl: GoodsRepository {

    override suspend fun getGoods(): List<Goods> {
        return listOf(Goods("234234234", 23232.0, "name"))
    }
}