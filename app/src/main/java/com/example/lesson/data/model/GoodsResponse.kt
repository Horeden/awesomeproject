package com.example.lesson.data.model

data class GoodsResponse(
    val name: String,
    val plu: Int
)