package com.example.lesson.presentation

sealed class Event {
    class RequestData : Event()
}
