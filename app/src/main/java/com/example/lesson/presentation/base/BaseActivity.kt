package com.example.lesson.presentation.base

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.example.lesson.presentation.Event
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.SendChannel
import kotlinx.coroutines.channels.actor
import kotlinx.coroutines.channels.consumeEach
import kotlin.coroutines.CoroutineContext

@ExperimentalCoroutinesApi
@ObsoleteCoroutinesApi
abstract class BaseActivity<TViewModel, TPresenter : BasePresenter<TViewModel>> : AppCompatActivity(), CoroutineScope,
    IView<TViewModel> {

    companion object {
        const val TAG: String = "TAG"
    }

    private val job: Job = Job()

    override val coroutineContext: CoroutineContext = job + Dispatchers.Main

    override val renderChannel: SendChannel<TViewModel> = actor(capacity = Channel.UNLIMITED) {
        consumeEach { currentModel ->
            render(currentModel)
        }
    }

    protected lateinit var presenter: TPresenter

    abstract fun createPresenter(): TPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        presenter = createPresenter()
        presenter.attachView(this)
    }

    protected abstract fun render(model: TViewModel)

    protected fun sendEvent(event: Event): Boolean = sendAnyEvent(event)

    private fun sendAnyEvent(event: Event): Boolean {
        return if (presenter.eventChannel.isClosedForSend)
            Log.d("TAG", "Event channel is closed for send.").let {
                false
            }
        else
            presenter.eventChannel.offer(event)
    }

    override fun onDestroy() {
        super.onDestroy()
        job.cancel()
        presenter.detachView()
    }

}