package com.example.lesson.presentation.goods

import com.example.lesson.domain.model.goods.Goods
import com.example.lesson.presentation.base.IView

interface IGoodsView : IView<IGoodsView.GoodsViewModel> {

    data class GoodsViewModel(
        val progress: Boolean = false,
        val list: List<Goods> = emptyList()
    )

}