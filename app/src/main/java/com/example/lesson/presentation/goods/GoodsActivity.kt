package com.example.lesson.presentation.goods

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.lesson.R
import com.example.lesson.data.repository.GoodsRepositoryImpl
import com.example.lesson.domain.interactor.GoodsInteractor
import com.example.lesson.presentation.Event
import com.example.lesson.presentation.base.BaseActivity
import com.example.lesson.presentation.goods.IGoodsView.GoodsViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
class GoodsActivity : BaseActivity<GoodsViewModel, GoodsPresenter>(), CoroutineScope {

    private lateinit var adapter: GoodsAdapter

    override fun render(model: GoodsViewModel) {
        adapter.setData(model.list)
    }

    @ExperimentalCoroutinesApi
    override fun createPresenter(): GoodsPresenter {
        return GoodsPresenter(GoodsInteractor(GoodsRepositoryImpl()))
    }

    @ExperimentalCoroutinesApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        sendEvent(Event.RequestData())
        adapter = GoodsAdapter(mutableListOf())
        recycler.adapter = adapter
        recycler.layoutManager = LinearLayoutManager(this)
    }
}

