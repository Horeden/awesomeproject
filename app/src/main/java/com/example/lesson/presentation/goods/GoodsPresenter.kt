package com.example.lesson.presentation.goods

import com.example.lesson.domain.interactor.GoodsInteractor
import com.example.lesson.presentation.Event
import com.example.lesson.presentation.base.BasePresenter
import com.example.lesson.presentation.goods.IGoodsView.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.ObsoleteCoroutinesApi

@ExperimentalCoroutinesApi
class GoodsPresenter (private val goodsInteractor: GoodsInteractor): BasePresenter<GoodsViewModel>(GoodsViewModel()) {

    override suspend fun handleEvent(event: Event) {
        when(event){
            is Event.RequestData -> handleGetData()
        }
    }

    @ObsoleteCoroutinesApi
    suspend fun handleEvent(event: GoodsViewModel) = handleGetData()

    @ObsoleteCoroutinesApi
    private suspend fun handleGetData() {
        val list = goodsInteractor.getGoods()
        val mutation = { viewModel: GoodsViewModel ->
            viewModel.copy(list = list)
        }
        applyMutation(mutation)
    }
}