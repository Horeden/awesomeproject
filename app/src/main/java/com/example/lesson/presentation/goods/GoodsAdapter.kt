package com.example.lesson.presentation.goods

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.lesson.R
import com.example.lesson.domain.model.goods.Goods
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.items.view.*

class GoodsAdapter(private var items: List<Goods>) : RecyclerView.Adapter<GoodsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.items, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    class ViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {
        fun bind(item: Goods) {
            containerView.tvPlu.text = item.plu
            containerView.tvName.text = item.name
        }
    }

    fun setData(items: List<Goods>) {
        this.items = items
        notifyDataSetChanged()
    }
}