package com.example.lesson.presentation.base

import kotlinx.coroutines.channels.SendChannel

interface IView<TViewModel> {

    val renderChannel: SendChannel<TViewModel>

}