package com.example.lesson.presentation.base

import androidx.lifecycle.ViewModel
import com.example.lesson.presentation.Event
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.actor
import kotlinx.coroutines.channels.consumeEach
import kotlin.coroutines.CoroutineContext

@ExperimentalCoroutinesApi
@ObsoleteCoroutinesApi
abstract class BasePresenter<TViewModel>(initialViewModel: TViewModel) : CoroutineScope, ViewModel() {

    private var view: IView<TViewModel>? = null

    private val job: Job = Job()

    override val coroutineContext: CoroutineContext = job + Dispatchers.Default

    private var currentViewModel: TViewModel = initialViewModel

    private val mutationChannel = actor<(TViewModel) -> TViewModel>(capacity = Channel.UNLIMITED) {
        consumeEach { mutator ->
            currentViewModel = mutator(initialViewModel)
            view?.renderChannel?.offer(currentViewModel)
        }
    }

    val eventChannel = actor<Event>(capacity = Channel.UNLIMITED) {
        consumeEach { event ->
            handleEvent(event)
        }
    }

    fun applyMutation(mutation: (TViewModel) -> TViewModel) {
        mutationChannel.offer(mutation)
    }

    override fun onCleared() {
        //
    }

    fun attachView(view: IView<TViewModel>) {
        this.view = view
    }

    fun detachView() {
        view = null
    }

    protected abstract suspend fun handleEvent(event: Event)

}